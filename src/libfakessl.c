/*
 * Copyright 2019 Lely Industries N.V.
 *
 * J. S. Seldenthuis <jseldenthuis@lely.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define _GNU_SOURCE

#include <dlfcn.h>

#include <openssl/x509.h>
#include <openssl/x509_vfy.h>

static _Thread_local int (*p_X509_verify_cert)(X509_STORE_CTX *ctx);
static _Thread_local int (*p_X509_STORE_CTX_get_error)(X509_STORE_CTX *ctx);
static _Thread_local void (*p_X509_STORE_CTX_set_error)(
		X509_STORE_CTX *ctx, int s);
static _Thread_local X509_STORE_CTX_verify_cb (*p_X509_STORE_CTX_get_verify_cb)(
		X509_STORE_CTX *ctx);
static _Thread_local void (*p_X509_STORE_CTX_set_verify_cb)(
		X509_STORE_CTX *ctx, X509_STORE_CTX_verify_cb verify);

// Pointer to the original user-defined verification callback.
static _Thread_local X509_STORE_CTX_verify_cb verify;

static int
fakessl_verify(int ok, X509_STORE_CTX *ctx)
{
	// Ignore the error if the certificate is not yet valid. This typically
	// happens if the clock was reset to Jan 1, 1970.
	if (p_X509_STORE_CTX_get_error(ctx) == X509_V_ERR_CERT_NOT_YET_VALID) {
		p_X509_STORE_CTX_set_error(ctx, X509_V_OK);
		ok = 1;
	}
	// Invoke the original user-defined verification callback, if set.
	return verify ? verify(ok, ctx) : ok;
}

int
X509_verify_cert(X509_STORE_CTX *ctx)
{
#ifdef __GNUC__
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
#endif
	// Load all functions dynamically, so we do not introduce an uncessary
	// dependency on libssl.so.
	if (!p_X509_verify_cert)
		p_X509_verify_cert = dlsym(RTLD_NEXT, "X509_verify_cert");
	if (!p_X509_STORE_CTX_get_error)
		p_X509_STORE_CTX_get_error =
				dlsym(RTLD_NEXT, "X509_STORE_CTX_get_error");
	if (!p_X509_STORE_CTX_set_error)
		p_X509_STORE_CTX_set_error =
				dlsym(RTLD_NEXT, "X509_STORE_CTX_set_error");
	if (!p_X509_STORE_CTX_get_verify_cb)
		p_X509_STORE_CTX_get_verify_cb = dlsym(
				RTLD_NEXT, "X509_STORE_CTX_get_verify_cb");
	if (!p_X509_STORE_CTX_set_verify_cb)
		p_X509_STORE_CTX_set_verify_cb = dlsym(
				RTLD_NEXT, "X509_STORE_CTX_set_verify_cb");
#ifdef __GNUC__
#pragma GCC diagnostic pop
#endif

	// Insert a custom verification callback.
	verify = p_X509_STORE_CTX_get_verify_cb(ctx);
	p_X509_STORE_CTX_set_verify_cb(ctx, &fakessl_verify);
	// Verify the certificate.
	int ok = p_X509_verify_cert(ctx);
	// Restore the original user-defined callback.
	p_X509_STORE_CTX_set_verify_cb(ctx, verify);
	verify = NULL;
	return ok;
}
